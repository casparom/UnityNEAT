﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour {
	private AbstractAgentFactory agentFactory;
	private Evolutionizer evolutionizer;
	private InnovationManager innovationManager;
	public GameObject moverPrefab;
	private bool isSimulating = false;
	public float timeScale = 1.0f;
	public int framesToSimulate = 500;
	public int currentFrame = 0;
	private int currentGeneration = -1;
	public NetworkViewer networkViewer;
	private SpeciesViewer speciesViewer;
	public NorthMoverAgent bestAgent;
	private float previousTimeScale = 1.0f;
	public Text generationCounterText;

	void Start () {
		networkViewer = GameObject.Find("BestNetworkViewer").GetComponent<NetworkViewer>();
		speciesViewer = GameObject.Find("SpeciesViewer").GetComponent<SpeciesViewer>();
		innovationManager = new InnovationManager ();
		agentFactory = new NorthMoverAgentFactory (innovationManager, moverPrefab);
		evolutionizer = new Evolutionizer (agentFactory);
		evolutionizer.setGenerationSize (100);
	}

	void Update() {
		if (Input.GetKeyDown ("space")) {
			toggleRunning ();
		}
		Time.timeScale = timeScale;
	}

	void FixedUpdate() {
		if (!isSimulating) {
			currentGeneration++;
			generationCounterText.text = "Generation: " + currentGeneration;
			Debug.Log ("Starting GENERATION: " + currentGeneration);
			currentFrame = 0;
			evolutionizer.createNewGeneration ();
			evolutionizer.speciate ();
			speciesViewer.renderSpecies (evolutionizer.AllSpecies);
			evolutionizer.dropSmallSpecies ();
			evolutionizer.startSimulating ();
			isSimulating = true;
			return;
		} else if (currentFrame < (int)(framesToSimulate / timeScale)) {
			currentFrame++;
			return;
		}
		isSimulating = false;
		evolutionizer.stopSimulating ();
		evolutionizer.dropUnfitMembers ();
		evolutionizer.dropEmptySpecies ();
		evolutionizer.calculateAdjustedFitnesses ();
		evolutionizer.dropStagnatedSpecies ();
		evolutionizer.dropWorstSpeciesIfTooMany ();
		evolutionizer.selectNewSpeciesMaskots ();
		handleBestAgent ();
	}

	private void handleBestAgent() {
		var currentBestAgent = (evolutionizer.getBestAgent () as NorthMoverAgent);
		if (currentBestAgent != bestAgent)
			bestAgent = currentBestAgent;
		bestAgent.AgentColor = Color.white;
		networkViewer.renderNetwork(bestAgent.Network);
		setNetworkViewerText ("Fitness: " + (int)bestAgent.fitness());
	}

	private void setNetworkViewerText(string text) {
		networkViewer.GetComponentInChildren<Text> ().text = text;
	}

	public void toggleRunning() {
		if (timeScale == 0.0f)
			timeScale = previousTimeScale;
		else {
			previousTimeScale = timeScale;
			timeScale = 0.0f;
		}
	}
}
