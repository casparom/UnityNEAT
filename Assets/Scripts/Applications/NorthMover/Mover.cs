﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class Mover : MonoBehaviour {
	private NeuralNetworkAgent agent;
	private NetworkViewer viewer;

	public void Start() {
		viewer = GameObject.Find("NetworkViewer").GetComponent<NetworkViewer>();
	}

	public void setAgent(NeuralNetworkAgent agent) {
		this.agent = agent;
	}

	public Transform getTransform() {
		return gameObject.transform;
	}

	void FixedUpdate () {
		var result = agent.Network.calculate (new double[] {gameObject.transform.rotation.z, gameObject.transform.position.x, gameObject.transform.position.y});

		double calculatedRotation = (result [0] + 1.0f) / 2.0f;
		var r = gameObject.transform.rotation;
		gameObject.transform.Rotate (new Vector3(r.x, r.y, (float)result[0]) * 10 * Time.timeScale);
		var p = gameObject.transform.position;
		var forwardSpeed = 0.0f;
		if (result[1] > 0)
			forwardSpeed = (float)result[1];
		gameObject.transform.Translate(Vector3.right * forwardSpeed * 0.1f * Time.timeScale);
		//gameObject.transform.position = new Vector2(p.x, p.y + (float)result [1]);
	}

	public void printArray(double[] array) {
		var contents = "";
		foreach (double item in array)
			contents += item +", ";
		Debug.Log (contents);
	}

	void OnMouseDown()
	{
		viewer.renderNetwork (agent.Network);
	}
}
