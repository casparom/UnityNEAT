﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NorthMoverAgent : NeuralNetworkAgent {
	private GameObject moverPrefab;
	private double fitnessScore = 0.0f;
	private Mover mover;
	private Color moverColor = Color.black;

	public override Color AgentColor {
		get {
			return moverColor;
		}
		set{
			moverColor = value;
			if (mover)
				mover.GetComponent<SpriteRenderer>().color = moverColor;
		}
	}

	public NorthMoverAgent(Genome genome, InnovationManager innovationManager, GameObject moverPrefab) : base(genome, innovationManager) {
		this.moverPrefab = moverPrefab;
	}

	public override AbstractAgent crossOver(AbstractAgent other) {
		return new NorthMoverAgent (crossGenomesOver (toThisType (other)), innovationManager, moverPrefab);
	}

	public override AbstractAgent copy() {
		return new NorthMoverAgent (genome.copy (), innovationManager, moverPrefab);
	}

	private NorthMoverAgent toThisType(AbstractAgent other) {
		if (!(other is NorthMoverAgent))
			throw new KeyNotFoundException ("Agent type is invalid");
		return (NorthMoverAgent)other;
	}

	public override void startSimulating() {
		mover = createMover ();
		mover.GetComponent<SpriteRenderer>().color = moverColor;
			var pos = mover.GetComponent<Transform> ().position;
		if (moverColor == Color.red || moverColor == Color.yellow)
			mover.GetComponent<Transform>().position = new Vector3(pos.x, pos.y, -1.0f);
		else
			mover.GetComponent<Transform>().position = new Vector3(pos.x, pos.y, 0f);
	}

	public override void stopSimulating() {
		fitnessScore = -mover.getTransform().position.x;
		MonoBehaviour.Destroy (mover.getTransform().gameObject);
	}

	private Mover createMover() {
		var m = MonoBehaviour.Instantiate (moverPrefab, new Vector2(0, 0), Quaternion.identity);
		var component = m.GetComponent<Mover> ();
		component.setAgent (this);
		var agentDetails = m.GetComponent<AgentDetails> ();
		agentDetails.setAgent (this);
		return component;
	}

	public override double fitness() {
		if (mover)
			return -mover.getTransform().position.x;
		return fitnessScore;
	}
}
