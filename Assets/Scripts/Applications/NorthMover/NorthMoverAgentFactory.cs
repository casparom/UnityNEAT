﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NorthMoverAgentFactory : AbstractAgentFactory {
	private InnovationManager innovationManager;
	private GameObject moverPrefab;

	public NorthMoverAgentFactory (InnovationManager innovationManager, GameObject moverPrefab) {
		this.innovationManager = innovationManager;
		this.moverPrefab = moverPrefab;
	}

	public override AbstractAgent createAgent() {
		return new NorthMoverAgent(Genome.createMinimumGenome(innovationManager.getOrCreateInitialMutation(3, 2)), innovationManager, moverPrefab);
	}
}
