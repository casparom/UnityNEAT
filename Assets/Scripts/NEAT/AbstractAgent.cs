﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractAgent {
	private Species species;

	public abstract void startSimulating ();
	public abstract void stopSimulating ();
	public abstract double fitness();
	public abstract double calculateCompatibilityDistance (AbstractAgent other);
	public abstract string name();
	public abstract AbstractAgent crossOver (AbstractAgent other);
	public abstract AbstractAgent copy ();
	public abstract void mutate();
	public abstract string toString();
	public Species AgentSpecies
	{
		get {
			return species;
		}
		set {
			species = value;
		}
	}
	public abstract Color AgentColor
	{
		get;
		set;
	}
}
