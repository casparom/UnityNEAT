﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractAgentFactory {
	public abstract AbstractAgent createAgent();
}
