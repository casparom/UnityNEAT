﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectionGene : Gene {
	private int inNode = 0;
	private int outNode = 0;
	private double weight = 1;
	private bool enabled = true;
	private bool isRecurrent = false;

	override public string toString() {
		return "Conn: {inNode:" + inNode + " outNode:" + outNode + " weight:" + weight + " enabled:" + enabled + " Inno:" + Innovation + ", isRecurrent: " + isRecurrent + "}"; 
	}

	public ConnectionGene(int inNode, int outNode, int innovation, bool isRecurrent) : base(innovation) {
		this.inNode = inNode;
		this.outNode = outNode;
		this.isRecurrent = isRecurrent;
		weight = GlobalRandom.nextDouble (2f) - 1;
	}

	public ConnectionGene(int inNode, int outNode, double weight, int innovation, bool isRecurrent) : base(innovation) {
		this.inNode = inNode;
		this.outNode = outNode;
		this.weight = weight;
		this.isRecurrent = isRecurrent;
	}

	public int InNode {
		get {
			return inNode;
		}
	}

	public int OutNode {
		get {
			return outNode;
		}
	}

	public double Weight {
		get {
			return weight;
		}
		set {
			weight = value;
		}
	}

	public bool Enabled {
		get {
			return enabled;
		}
		set {
			enabled = value;
		}
	}

	public bool IsRecurrent {
		get {
			return isRecurrent;
		}
	}

	public override void randomize () {
		weight = NeatUtils.randomize (weight);
	}

	public override double calculateAbsoluteWeightDifference (Gene other) {
		if (!(other is ConnectionGene))
			throw new KeyNotFoundException ("FATAL: This is not a compatible gene");

		var o = other as ConnectionGene;
		return Math.Abs (weight - o.weight);
	}

	override public Gene copy() {
		var result = new ConnectionGene (inNode, outNode, Innovation, isRecurrent);
		result.Weight = Weight;
		result.enabled = enabled;
		return result;
	}

}
