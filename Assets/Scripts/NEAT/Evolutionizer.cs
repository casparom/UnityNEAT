﻿
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;


public class Evolutionizer {
	private AbstractAgentFactory agentFactory;
	private HashSet<AbstractAgent> currentGeneration = new HashSet<AbstractAgent>();
	private HashSet<Species> allSpecies = new HashSet<Species>();
	private int generationSize = 100;
	private double speciesToLeaveAfterPruneFactor = 0.3f;
	private double compatibilityThreshold = 0.5f;
	private int speciesMemberCountThreshold = 0;
	private int maxNumberOfSpecies = 20;
	private int stagnationThreshold = 20;

	public Evolutionizer(AbstractAgentFactory agentFactory) {
		this.agentFactory = agentFactory;
	}

	public HashSet<Species> AllSpecies {
		get {
			return allSpecies;
		}
	}

	public void setGenerationSize(int size) {
		generationSize = size;
	//	speciesMemberCountThreshold = generationSize / 30;
	}

	public void startSimulating() {
		foreach (AbstractAgent agent in currentGeneration) {
			agent.startSimulating ();
		}
	}

	public void stopSimulating() {
		foreach (AbstractAgent agent in currentGeneration) {
			agent.stopSimulating ();
		}
	}

	public void speciate() {
		foreach (Species species in allSpecies)
			species.clearMembers ();
		foreach (AbstractAgent agent in currentGeneration) {
			bool found = false;
			foreach (Species species in allSpecies) {
				if (agent == species.Maskot) {
					species.addMember (agent);
					found = true;
					break;
				}
			}
			foreach (Species species in allSpecies) {
				var compatibilityDistance = agent.calculateCompatibilityDistance (species.Maskot);
			//	Debug.Log ("Compatibility distance: " + compatibilityDistance);
				if (compatibilityDistance < compatibilityThreshold) {
					species.addMember (agent);
					found = true;
					break;
				}
			}
			if (!found)
				allSpecies.Add(createSpecies(agent));
		}
		dropEmptySpecies ();
		Debug.Log("Number of species: " + allSpecies.Count);
		Debug.Log ("Species after speciation:");
		foreach (Species species in allSpecies)
			Debug.Log (species.toString ());
	}

	private Species createSpecies(AbstractAgent agent) {
		return new Species (agent);
	}

	public void calculateAdjustedFitnesses() {
		foreach (Species species in allSpecies) 
			species.calculateAdjustedFitnesses ();
	}

	public void dropUnfitMembers() {
		foreach (Species species in allSpecies)
			species.dropUnfitMembers ();
	}

	public void printSpecies() {
		foreach (Species species in allSpecies) {
			species.toString ();
		}
	}

	public void createNewGeneration() {
		if (currentGeneration.Count == 0) {
			initialize ();
			return;
		}
		currentGeneration.Clear ();
		foreach (Species species in allSpecies) {
			foreach (AbstractAgent agent in species.Members) {
				currentGeneration.Add (agent);
			}
		}
		while (currentGeneration.Count < generationSize) {
			if (allSpecies.Count < 1)
				throw new Exception ("CAN NOT BE");
			var speciesPair = Species.SelectPseudoRandomly (allSpecies);
			AbstractAgent parent1 = speciesPair.Key.getRandomMember ();
			AbstractAgent parent2 = speciesPair.Value.getRandomMember ();
			AbstractAgent newAgent;
			if (parent1 == parent2) {
				newAgent = parent1.copy ();
			} else {
				if (parent1.fitness () >= parent2.fitness ())
					newAgent = parent1.crossOver (parent2);
				else
					newAgent = parent2.crossOver (parent1);
			}
			newAgent.mutate ();
			Debug.Log ("Parent is: " + parent1.name ());
			Debug.Log ("Parent is: " + parent2.name ());
			currentGeneration.Add (newAgent);
		}
		Debug.Log ("Current generation size: " + currentGeneration.Count);
	}

	public void selectNewSpeciesMaskots() {
		foreach (Species species in allSpecies) {
			species.selectRandomMaskotFromMembers ();
		}
	}

	private void initialize() {
		for (int i = 0; i < generationSize; i++) {
			currentGeneration.Add (agentFactory.createAgent());
		}
	}

	//millegipärast praegu ei tööta normaalselt. peale kolmandat generatsiooni on kõik samasugused.

	public void dropWorstSpeciesIfTooMany() {
		if (allSpecies.Count <= 1 || allSpecies.Count < maxNumberOfSpecies)
			return;

		var currentSpecies = new List<Species>(allSpecies);
		currentSpecies.Sort ((x, y) => y.Fitness.CompareTo(x.Fitness));
		for (int i = maxNumberOfSpecies; i < currentSpecies.Count; i++)
			removeSpecies (currentSpecies [i]);
	}

	private void removeSpecies(Species species) {
		Debug.Log ("Removing species " + species.toString());
		foreach (AbstractAgent agent in species.Members)
			currentGeneration.Remove (agent);
		allSpecies.Remove (species);
	}

	public void dropEmptySpecies() {
		if (allSpecies.Count <= 1)
			return;
		var currentSpecies = new HashSet<Species> (allSpecies);
		var bestSpecies = getBestSpecies ();
		foreach (Species species in currentSpecies)
			if (species.memberCount() == 0 && species != bestSpecies)
				removeSpecies (species);
	}

	public void dropSmallSpecies() {
		if (allSpecies.Count <= 1)
			return;
		var currentSpecies = new HashSet<Species> (allSpecies);
		var bestSpecies = getBestSpecies ();
		Debug.Log ("Species member count threshold: " + speciesMemberCountThreshold);
		foreach (Species species in currentSpecies)
			if (species.memberCount () < speciesMemberCountThreshold && species != bestSpecies)
				removeSpecies (species);
	}

	public void dropStagnatedSpecies() {
		if (allSpecies.Count <= 1)
			return;
		var currentSpecies = new HashSet<Species> (allSpecies);
		var bestSpecies = getBestSpecies ();
		foreach (Species species in currentSpecies)
			if (species.StagnatedCount >= stagnationThreshold && species != bestSpecies)
				removeSpecies (species);
	}

	private Species getBestSpecies() {
		var bestSpecies = allSpecies.First();
		foreach (Species species in allSpecies)
			if (species.Fitness > bestSpecies.Fitness)
				bestSpecies = species;
		return bestSpecies;
	}

	public AbstractAgent getBestAgent() {
		AbstractAgent result = currentGeneration.First();
		foreach (AbstractAgent agent in currentGeneration)
			if (agent.fitness () > result.fitness())
				result = agent;
		return result;
	}
}
