﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Gene {
	private int innovation = 0;

	public Gene(int innovation) {
		this.innovation = innovation;
	}

	public int Innovation {
		get {
			return innovation;
		}
	}

	public abstract void randomize ();
	public abstract double calculateAbsoluteWeightDifference (Gene other);
	public abstract Gene copy ();
	public abstract string toString();
}
