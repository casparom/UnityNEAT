﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Genome {
	private static int idCounter = 0;
	private SortedDictionary<int, NodeGene> nodes = new SortedDictionary<int, NodeGene>();
	private SortedDictionary<int, ConnectionGene> connections = new SortedDictionary<int, ConnectionGene>();
	private SortedDictionary<int, Gene> genes = new SortedDictionary<int, Gene>();
	private List<Mutation> mutations = new List<Mutation>();
	private int id = idCounter++;

	public Genome(Mutation initialMutation) {
		initialMutation.apply (this);
		mutations.Add (initialMutation);
	}

	private Genome(List<Gene> genes, List<Mutation> mutations) {
		setGenes (genes);
		foreach (Mutation mutation in mutations)
			this.mutations.Add (mutation.copy ());
	}

	public List<Gene> Genes {
		get {
			return genes.Values.ToList();
		}
	}

	public Genome copy() {
		return new Genome(copyGenes(), mutations);
	}

	private List<Gene> copyGenes() {
		List<Gene> copys = new List<Gene>();
		foreach (Gene gene in genes.Values)
			copys.Add (gene.copy ());
		return copys;
	}

	public int Id {
		get {
			return id;
		}
	}

	public static Genome createMinimumGenome(InitialMutation mutation) {
		return new Genome (mutation);
	}

	public void setGenes(List<Gene> genes) {
		foreach (Gene gene in genes) {
			this.genes.Add (gene.Innovation, gene);
			if (gene is NodeGene)
				nodes.Add (gene.Innovation, gene as NodeGene);
			else if (gene is ConnectionGene)
				connections.Add (gene.Innovation, gene as ConnectionGene);
			else
				throw new KeyNotFoundException ("SOME WEIRD TYPE OF GENE!!!");
		}
	}

	public SortedDictionary<int, NodeGene> Nodes {
		get {
			return nodes;
		}
	}

	public SortedDictionary<int, ConnectionGene> Connections {
		get {
			return connections;
		}
	}

	public List<ConnectionGene> getEnabledNonRecurrentConnections() {
		return connections.Values.ToList ().FindAll (c => c.Enabled && !c.IsRecurrent);
	}

	public List<ConnectionGene> getNonRecurrentConnections() {
		return connections.Values.ToList ().FindAll (c => !c.IsRecurrent);
	}

	public void print() {
		Debug.Log (toString());
	}

	public string toString() {
		string result = "";
		foreach (Gene gene in genes.Values)
			result += gene.toString () + "; ";
		foreach (Mutation mutation in mutations)
			result += mutation.toString () + "; ";
		return result;
	}

	public bool hasGeneWithInnovation(int innovation) {
		return genes.ContainsKey (innovation);
	}

	public Genome crossOver(Genome other) {
		int previous = -1;
		foreach (int innovation in genes.Keys) {
			if (innovation <= previous)
				throw new KeyNotFoundException ("NOT SORTED!!!!");
			previous = innovation;
		}
		List<Gene> resultGenes = new List<Gene> ();
		foreach (int inno in genes.Keys) {
			if (other.genes.ContainsKey (inno)) {
				resultGenes.Add(GlobalRandom.nextBool () ? genes [inno].copy() : other.genes [inno].copy());
			}
			else {
				resultGenes.Add (genes [inno].copy ());
			}
		}
		var child = new Genome (resultGenes, mutations);
		return child;
	}

	public void addNode(NodeGene gene) {
		nodes.Add (gene.Innovation, gene);
		genes.Add (gene.Innovation, gene);
	}

	public void addConnection(ConnectionGene gene) {
		connections.Add (gene.Innovation, gene);
		genes.Add (gene.Innovation, gene);
	}

	public void addMutation(Mutation mutation) {
		mutations.Add (mutation);
	}

	public void enableConnection(int inno) {
		connections [inno].Enabled = true;
	}

	public double calculateCompatibilityDistance (Genome other) {
		var C1 = 1.0f;
		var C2 = 1.0f;
		var C3 = 0.4f;
		var disjointedCount = 0;
		var exessCount = 0;
		int thisLast = genes.Last ().Key;
		int otherLast = other.genes.Last ().Key;
		Dictionary<Gene, Gene> matchingGenes = new Dictionary<Gene, Gene> ();
		for (int i = 0; i < genes.Count; i++) {
			if (genes.ElementAt(i).Value.Innovation > otherLast) {
				exessCount += (genes.Count - i);
				break;
			}
			var key = genes.ElementAt (i).Key;
			if (!other.genes.ContainsKey (key))
				disjointedCount++;
			else
				matchingGenes.Add (genes[key], other.genes[key]);
		}
		for (int i = 0; i < other.genes.Count; i++) {
			if (other.genes.ElementAt(i).Value.Innovation > thisLast) {
				exessCount += (other.genes.Count - i);
				break;
			}
			if (!genes.ContainsKey(other.genes.ElementAt(i).Key))
				disjointedCount++;
		}
		double averageWeightDifference = 0.0f;
		if (matchingGenes.Count != 0) {
			foreach (KeyValuePair<Gene, Gene> pair in matchingGenes)
				averageWeightDifference += pair.Key.calculateAbsoluteWeightDifference (pair.Value);
			averageWeightDifference /= matchingGenes.Count;
		}
		var normalizingFactor = Math.Sqrt(Math.Max (genes.Count, other.genes.Count));
		var compatibilityDistance = C1 * disjointedCount / normalizingFactor + C2 * exessCount / normalizingFactor + C3 * averageWeightDifference;
		//Debug.Log ("this: " + toString() + " other: " + other.toString() + " disjointed: " + disjointedCount + " exess: " + exessCount + " weightDifference: " + averageWeightDifference + " + normalizing factor: " + normalizingFactor + " distance: " + compatibilityDistance);
		return compatibilityDistance;
	}

	public ConnectionGene getConnectionBetween(int srcNode, int destNode) {
		foreach (ConnectionGene connection in connections.Values)
			if (connection.InNode == srcNode && connection.OutNode == destNode)
				return connection;

		throw new MemberAccessException("There is no connection between + " + srcNode + " and " + destNode);
	}

	public void randomize() {
		// TODO: implement
	}
}
