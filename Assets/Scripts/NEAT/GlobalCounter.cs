﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalCounter {
	private static int number = 1;

	public static int getNext() {
		return number++;
	}

	public static int current() {
		return number;
	}

	public static void reset() {
		number = 1;
	}
}
