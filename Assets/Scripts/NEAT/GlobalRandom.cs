﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalRandom {
	private static System.Random instance = new System.Random(0);

	public static int nextInt() {
		return instance.Next ();
	}

	public static int nextInt(int max) {
		return instance.Next(max);
	}

	public static int nextInt(int min, int max) {
		return instance.Next (min, max);
	}

	public static double nextDouble(double max) {
		return instance.NextDouble () * max;
	}

	public static bool nextBool() {
		return instance.Next (2) == 1;
	}
}
