﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InnovationManager {
	private List<Mutation> mutations = new List<Mutation>();

	public InitialMutation getOrCreateInitialMutation (int numberOfInputs, int numberOfOutputs)
	{
		var mutation = new InitialMutation (numberOfInputs, numberOfOutputs);
		if (mutations.Contains(mutation)) {
			mutation = (InitialMutation)mutations.Find (m=>m.Equals(mutation));
		} else {
			mutation.setInnovation(GlobalCounter.getNext());
			var numberOfGenes = mutation.getNumberOfGenes ();
			for (int i = 1; i < numberOfGenes; i++)
				GlobalCounter.getNext();
			mutations.Add(mutation);
		}
		return mutation;
	}

	public AddNodeMutation getOrCreateAddNodeMutation(int connection, int srcNode, int destNode, double weight) {
		var mutation = new AddNodeMutation (connection, srcNode, destNode, weight);
		if (mutations.Contains (mutation)) {
			mutation = (AddNodeMutation)mutations.Find (m=>m.Equals(mutation));
		} else {
			mutation.setInnovation(GlobalCounter.getNext ());
			GlobalCounter.getNext ();
			GlobalCounter.getNext ();
			mutations.Add(mutation);
		}
		return mutation;
	}

	public AddConnectionMutation getOrCreateAddConnectionMutation(int srcNode, int destNode) {
		return doGetOrCreateAddConnectionMutation(srcNode, destNode, false);
	}

	public AddConnectionMutation getOrCreateAddRecurrentConnectionMutation(int srcNode, int destNode) {
		return doGetOrCreateAddConnectionMutation(srcNode, destNode, true);
	}

	private AddConnectionMutation doGetOrCreateAddConnectionMutation(int srcNode, int destNode, bool isRecurrent) {
		var mutation = new AddConnectionMutation (srcNode, destNode, isRecurrent);
		if (mutations.Contains (mutation)) {
			mutation = (AddConnectionMutation)mutations.Find (m=>m.Equals(mutation));
		} else {
			mutation.setInnovation(GlobalCounter.getNext ());
			mutations.Add (mutation);
		}
		return mutation;
	}

	public EnableConnectionMutation getOrCreateEnableConnectionMutation(int connection) {
		return new EnableConnectionMutation (connection);
	}
}
