﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Mutation {
	public abstract bool isApplicableTo (Genome genome);
	public abstract Mutation copy ();
	public abstract void apply (Genome genome);
	public abstract string toString();
}

public class InitialMutation : Mutation {
	private int numberOfInputs;
	private int numberOfOutputs;
	private int numberOfGenes;
	private int innovation;

	public InitialMutation (int numberOfInputs, int numberOfOutputs) {
		this.numberOfInputs = numberOfInputs;
		this.numberOfOutputs = numberOfOutputs;
	}

	public override Mutation copy() {
		var result = new InitialMutation (numberOfInputs, numberOfOutputs);
		result.innovation = innovation;
		result.numberOfGenes = numberOfGenes;
		return result;
	}

	public void setInnovation(int innovation) {
		this.innovation = innovation;
	}

	public override void apply(Genome genome) {
		var tempInno = innovation;
		var genes = new List<Gene>();
		for (int i = 0; i < numberOfInputs; i++)
			genes.Add(new NodeGene (tempInno++, NodeGene.TYPE.INPUT));
		for (int i = 0; i < numberOfOutputs; i++)
			genes.Add(new NodeGene (tempInno++, NodeGene.TYPE.OUTPUT));
		for (int i = 0; i < numberOfInputs; i++)
			for (int j = numberOfInputs; j < numberOfInputs + numberOfOutputs; j++)
				genes.Add (new ConnectionGene (genes[i].Innovation, genes[j].Innovation, tempInno++, false));
		genome.setGenes (genes);
		genome.addMutation (this);
	}

	override public bool Equals(object other) {
		if (other == null || (other.GetType() != this.GetType()))
			return false;
		var o = (InitialMutation)other;
		return numberOfInputs == o.numberOfInputs && numberOfOutputs == o.numberOfOutputs;
	}

	public override string toString() {
		return "InitialMutation: numberOfInputs - " + numberOfInputs + " numberOfOutputs - " + numberOfOutputs + " innovation - " + innovation;
	}

	public int getNumberOfGenes() {
		if (numberOfGenes == 0) {
			numberOfGenes = 0;
			numberOfGenes += numberOfInputs;
			numberOfGenes += numberOfOutputs;
			numberOfGenes += numberOfInputs * numberOfOutputs;
		}
		return numberOfGenes;
	}

	public override bool isApplicableTo(Genome genome) {
		return !genome.hasGeneWithInnovation (innovation);
	}
}

public class AddNodeMutation : Mutation {
	private int oldConnection;
	private int srcNode;
	private int destNode;
	private double weight;
	private int innovation = -1;

	public AddNodeMutation(int oldConnection, int srcNode, int destNode, double weight) {
		this.oldConnection = oldConnection;
		this.srcNode = srcNode;
		this.destNode = destNode;
		this.weight = weight;
	}
	
	public override Mutation copy() {
		var result = new AddNodeMutation (oldConnection, srcNode, destNode, weight);
		result.innovation = innovation;
		return result;
	}
	
	public override void apply(Genome genome) {
		if (innovation == -1)
			throw new KeyNotFoundException ("Innovation has not been set");
		genome.Connections [oldConnection].Enabled = false;
		var newNode = new NodeGene (innovation);
		var inConnection = new ConnectionGene (srcNode, innovation, 1, innovation + 1, false);
		var outConnection = new ConnectionGene (innovation, destNode, weight, innovation + 2, false);
		genome.addNode (newNode);
		genome.addConnection(inConnection);
		genome.addConnection(outConnection);
		genome.addMutation (this);
	}

	public void setInnovation(int innovation) {
		this.innovation = innovation;
	}

	override public bool Equals(object other) {
		if (other == null || (other.GetType() != this.GetType()))
			return false;
		var o = (AddNodeMutation)other;
		return oldConnection == o.oldConnection && srcNode == o.srcNode && destNode == o.destNode;
	}

	public override string toString() {
		return "AddNodeMutation: oldConnection - " + oldConnection + " innovation - " + innovation;
	}

	public override bool isApplicableTo(Genome genome) {
		return !genome.hasGeneWithInnovation (innovation);
	}
}

public class AddConnectionMutation : Mutation {
	private int srcNode;
	private int destNode;
	private int innovation = -1;
	private bool isRecurrent = false;

	public AddConnectionMutation(int srcNode, int destNode, bool isRecurrent) {
		this.srcNode = srcNode;
		this.destNode = destNode;
		this.isRecurrent = isRecurrent;
	}

	public override Mutation copy() {
		var result = new AddConnectionMutation (srcNode, destNode, isRecurrent);
		result.innovation = innovation;
		return result;
	}

	public override void apply(Genome genome) {
		if (innovation == -1)
			throw new KeyNotFoundException ("Innovation has not been set");
		var newConnection = new ConnectionGene (srcNode, destNode, innovation, isRecurrent);
		genome.addConnection (newConnection);
		genome.addMutation (this);
	}

	public void setInnovation(int innovation) {
		this.innovation = innovation;
	}

	override public bool Equals(object other) {
		if (other == null || (other.GetType() != this.GetType()))
			return false;
		var o = (AddConnectionMutation)other;
		return srcNode == o.srcNode && destNode == o.destNode && isRecurrent == o.isRecurrent;
	}

	public override string toString() {
		return "AddConnectionMutation: innovation - " + innovation + " recurrent: " + isRecurrent;
	}

	public override bool isApplicableTo(Genome genome) {
		return !genome.hasGeneWithInnovation (innovation);
	}
}

public class EnableConnectionMutation : Mutation {
	private int connection = 0;

	public EnableConnectionMutation(int connection) {
		this.connection = connection;
	}

	public override Mutation copy() {
		return new EnableConnectionMutation (connection);
	}

	public override void apply(Genome genome) {
		genome.enableConnection (connection);
		genome.addMutation (this);
	}

	override public bool Equals(object other) {
		if (other == null || (other.GetType() != this.GetType()))
			return false;
		var o = (EnableConnectionMutation)other;
		return connection == o.connection;
	}

	public override string toString() {
		return "EnableConnectionMutation: connection - " + connection;
	}

	public override bool isApplicableTo(Genome genome) {
		return true;
	}
}

public class RandomizeWeightsAndBiasesMutation : Mutation {
	public override Mutation copy() {
		return new RandomizeWeightsAndBiasesMutation ();
	}

	public override void apply(Genome genome) {
		foreach (Gene gene in genome.Genes)
			gene.randomize ();
		genome.addMutation (this);
	}

	override public bool Equals(object other) {
		if (other == null || (other.GetType() != this.GetType()))
			return false;
		return false;
	}

	public override string toString() {
		return "RandomizeWeightsAndBiasesMutation";
	}

	public override bool isApplicableTo(Genome genome) {
		return true;
	}
}

public class NullMutation : Mutation {
	public override void apply(Genome genome) {
		genome.addMutation (this);
	}

	override public bool Equals(object other) {
		if (other == null || (other.GetType() != this.GetType()))
			return false;
		return true;
	}

	public override string toString() {
		return "NullMutation";
	}

	public override Mutation copy() {
		return new NullMutation ();
	}

	public override bool isApplicableTo(Genome genome) {
		return true;
	}
}
