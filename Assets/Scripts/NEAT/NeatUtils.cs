﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


class NeatUtils {
	public static double getPseudoRandom(List<double> cumulativeSums) {
		var maxSum = cumulativeSums[cumulativeSums.Count-1];
		var randomValue = GlobalRandom.nextDouble (maxSum);
		foreach (double cumulativeSum in cumulativeSums) {
			if (randomValue <= cumulativeSum) {
				return cumulativeSum;
			}
		}
		throw new ApplicationException ("This code should never be reached");
	}

	public static double randomize(double value) {
		var proportions = new List<int> (){40, 5, 5, 5, 5, 20};
		var sum = 0;
		foreach (int proportion in proportions)
			sum += proportion;
		var randomNumber = GlobalRandom.nextInt (0, sum);
		var runningSum = 0;
		var method = 0;
		for (int i = 0; i < proportions.Count; i++) {
			runningSum += proportions[i];
			if (randomNumber < runningSum) {
				method = i;
				break;
			}
		}

		switch (method) {
		case 0:
			return value * GlobalRandom.nextDouble (1f) - 0.5f;
		case 1:
			return value *= -1;
		case 2:
			return 1;
		case 3:
			return -1;
		case 4:
			return 0;
		case 5:
			return value;
		default:
			return value;
		}
	}
}