﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public abstract class NeuralNetworkAgent : AbstractAgent {
	protected Genome genome;
	private static int idCounter = 0;
	public string itsName = "";
	private int id = idCounter++;
	protected NeuralNetwork network;
	protected InnovationManager innovationManager = null;

	public NeuralNetworkAgent(Genome genome, InnovationManager innovationManager) {
		this.innovationManager = innovationManager;
		this.genome = genome;
		createNetwork ();
	}

	public Genome Genome {
		get {
			return genome;
		}
	}

	public NeuralNetwork Network {
		get {
			return network;
		}
	}

	public override double calculateCompatibilityDistance (AbstractAgent other) {
		return genome.calculateCompatibilityDistance (toThisType(other).genome);
	}

	private NeuralNetworkAgent toThisType(AbstractAgent other) {
		if (!(other is NeuralNetworkAgent))
			throw new KeyNotFoundException ("Agent type is invalid");
		return (NeuralNetworkAgent)other;
	}

	public override string name() {
		if (itsName == "")
			itsName = id + "-" + genome.Id;
		return itsName;
	}

	protected Genome crossGenomesOver(NeuralNetworkAgent other) {
		return genome.crossOver (toThisType (other).genome);
	}

	public override void mutate() {
		var mutations = getMutationsBasedOnChance ();
		foreach (Mutation mutation in mutations)
			if (mutation.isApplicableTo (genome))
				mutation.apply (genome);
		createNetwork ();
	}

	private void createNetwork() {
		network = new NeuralNetwork (genome);
	}

	private List<Mutation> getMutationsBasedOnChance() {
		var addNodeMutationChance = 5;
		var addConnectionMutationChance = 5;
		var randomizeMutationChance = 80;
		var mutations = new List<Mutation> ();
		if (GlobalRandom.nextInt (0, 100) < addNodeMutationChance)
			mutations.Add (getAddNodeMutation());
		if (GlobalRandom.nextInt (0, 100) < addConnectionMutationChance)
			mutations.Add (getAddConnectionMutation());
		if (GlobalRandom.nextInt (0, 100) < randomizeMutationChance)
			mutations.Add (getRandomizeMutation());

		return mutations;
	}

	public Mutation getAddConnectionMutation() {
		var nodes = network.Genome.Nodes;
		var connections = network.Genome.Connections;
		for (int i = 0; i < 10; i++) { // tries n number of times
			var srcNode = nodes.ElementAt(GlobalRandom.nextInt(nodes.Count)).Key;
			var destNode = nodes.ElementAt(GlobalRandom.nextInt(nodes.Count)).Key;
			bool exists = false;
			int disabledCon = 0;
			foreach (ConnectionGene con in connections.Values) {
				if (con.InNode == srcNode && con.OutNode == destNode) {
					exists = true;
					if (!con.Enabled)
						disabledCon = con.Innovation;
					break;
				}
				else if (con.InNode == destNode && con.OutNode == srcNode) {
					exists = true;
					if (!con.Enabled)
						disabledCon = con.Innovation;
					else {
						var temp = srcNode;
						srcNode = destNode;
						destNode = temp;
					}
					break;
				}
			}
			if (!exists) {
				if (network.layerOf(srcNode) < network.layerOf(destNode)) 
					return innovationManager.getOrCreateAddConnectionMutation (srcNode, destNode);
				else
					return innovationManager.getOrCreateAddRecurrentConnectionMutation (srcNode, destNode);
			}
			else if (disabledCon != 0)
				return innovationManager.getOrCreateEnableConnectionMutation (disabledCon);
		}
		return new NullMutation();
	}

	public Mutation getAddNodeMutation() {
		var connections = genome.getEnabledNonRecurrentConnections();
		if (connections.Count == 0)
			connections = genome.getNonRecurrentConnections ();
		var con = connections[GlobalRandom.nextInt(connections.Count)];
		return innovationManager.getOrCreateAddNodeMutation (con.Innovation, con.InNode, con.OutNode, con.Weight);
	}

	public Mutation getRandomizeMutation() {
		return new RandomizeWeightsAndBiasesMutation ();
	}

	public override string toString() {
		return name () + " fitness: " + fitness ();
	}
}
