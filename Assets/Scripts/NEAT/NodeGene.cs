﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeGene : Gene {
	public enum TYPE { INPUT, OUTPUT, HIDDEN };

	private TYPE type = TYPE.HIDDEN;
	private double bias = 0.0f;

	public TYPE Type {
		get {
			return type;
		}
	}

	public double Bias {
		get {
			return bias;
		}
	}

	public NodeGene(int innovation, TYPE type = TYPE.HIDDEN) : base(innovation) {
		this.type = type;
		bias = GlobalRandom.nextDouble (2f) - 1;
	}

	public override void randomize () {
		bias = NeatUtils.randomize (bias);
	}

	public override double calculateAbsoluteWeightDifference (Gene other) {
		if (!(other is NodeGene))
			throw new KeyNotFoundException ("FATAL: This is not a compatible gene");
		return 0;
	}

	override public Gene copy() {
		var result = new NodeGene (Innovation, type);
		result.bias = bias;
		return result;
	}

	override public string toString() {
		return "Node: {Type:" + type.ToString () + " Inno:" + Innovation + "}"; 
	}
}
