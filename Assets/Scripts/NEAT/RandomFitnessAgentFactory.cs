﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class RandomFitnessAgent : AbstractAgent {
	private double fitnessScore = 0.0f;

	public RandomFitnessAgent() {
		fitnessScore = (double)GlobalRandom.nextInt (0, 1000);
	}

	private RandomFitnessAgent(double fitnessScore) {
		this.fitnessScore = fitnessScore;
	}

	public override Color AgentColor {
		get {
			return Color.black;
		}
		set{}
	}

	public override void startSimulating () {
	}
	public override void stopSimulating () {
	}
	public override double fitness() {
		return fitnessScore;
	}
	public override double calculateCompatibilityDistance (AbstractAgent other) {
		return 0;
	}
	public override string name() {
		return "" + fitness();
	}
	public override AbstractAgent crossOver (AbstractAgent other) {
		return new RandomFitnessAgent(0.5 * (fitness() + other.fitness()));
	}
	public override AbstractAgent copy () {
		return new RandomFitnessAgent (fitness ());
	}
	public override void mutate() {
		fitnessScore = NeatUtils.randomize (fitnessScore);
	}
	public override string toString() {
		return name ();
	}
}

public class RandomFitnessAgentFactory : AbstractAgentFactory {
	public override AbstractAgent createAgent() {
		return new RandomFitnessAgent();
	}
}
