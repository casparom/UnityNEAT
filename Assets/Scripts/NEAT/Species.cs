﻿
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Species {
	private string name;
	private AbstractAgent maskot;
	private HashSet<AbstractAgent> members = new HashSet<AbstractAgent>();
	private double fitness = 0;
	private int stagnatedCount = 0;
	private AbstractAgent bestAgent;

	private int minContinuationNumber = 1;
	private double continuationPercentile = 0.2f;
	private int maxContinuationCount = 5;
	private Color color = UnityEngine.Random.ColorHSV();

	public static KeyValuePair<Species, Species> SelectPseudoRandomly(HashSet<Species> optionSet) {
		var options = optionSet.ToList ();
		double fitnessSum = 0.0f;
		var sumsToSpeciesIndexesMap = new Dictionary<double, int> ();
		for (int i = 0; i < options.Count; i++) {
			if (options [i].fitness <= 0)
				fitnessSum += 0.1f;
			else
				fitnessSum += options[i].fitness;
			sumsToSpeciesIndexesMap.Add (fitnessSum, i);
		}
		var cumulativeSumsList = sumsToSpeciesIndexesMap.Keys.ToList();
		var species1 = options [sumsToSpeciesIndexesMap [NeatUtils.getPseudoRandom (cumulativeSumsList)]];
		var species2 = options [sumsToSpeciesIndexesMap [NeatUtils.getPseudoRandom (cumulativeSumsList)]];
		return new KeyValuePair<Species, Species>(species1, species2);
	}

	public double Fitness {
		get {
			return fitness;
		}
	}

	public int StagnatedCount {
		get {
			return stagnatedCount;
		}
	}

	public Color Color {
		get {
			return color;
		}
	}

	public Species(AbstractAgent maskot) {
		this.maskot = maskot;
		this.name = "SPC-" + maskot.name();
		addMember (maskot);
		bestAgent = maskot;
		maskot.AgentSpecies = this;
	}

	public void addMember(AbstractAgent agent) {
		members.Add (agent);
		agent.AgentColor = color;
		agent.AgentSpecies = this;
	}

	public string Name {
		get {
			return name;
		}
	}

	public AbstractAgent Maskot {
		get {
			return maskot;
		}
	}

	public ICollection<AbstractAgent> Members {
		get {
			return members;
		}
	}

	public int memberCount() {
		return members.Count;
	}

	public void clearMembers() {
		members.Clear ();
	}

	public void calculateAdjustedFitnesses() {
		fitness = 0;
		var currentBestAgent = members.First();
		foreach (AbstractAgent agent in members) {
			if (agent.fitness () > currentBestAgent.fitness()) {
				currentBestAgent = agent;
			}
			fitness += (agent.fitness () / memberCount ());
		}

		if (currentBestAgent.fitness() > bestAgent.fitness()) {
			stagnatedCount = 0;
			bestAgent = currentBestAgent;
		} else if (currentBestAgent.fitness() == bestAgent.fitness()) {
			stagnatedCount++;
		} else {
			throw new Exception (toString() + " best fitness has reduced. previous: " + bestAgent.toString() + " new: " + currentBestAgent.toString());
		}
	}

	public void dropUnfitMembers () {
		Debug.Log ("Droopping unfit members " + toString());
		if (memberCount() <= 1)
			return;
		var currentBestAgent = members.First();
		var bestFitness = Double.MinValue;
		var worstFitness = Double.MaxValue;
		foreach (AbstractAgent agent in members) {
			if (agent.fitness () < worstFitness)
				worstFitness = agent.fitness ();
			if (agent.fitness () > bestFitness) {
				bestFitness = agent.fitness ();
				currentBestAgent = agent;
			}
		}
		var lowestSuitableFitness = bestFitness - ((bestFitness - worstFitness) * continuationPercentile);
		var fitMembers = new List<AbstractAgent>();
		foreach (AbstractAgent agent in members) {
			if (agent.fitness() >= lowestSuitableFitness)
				fitMembers.Add(agent);
		}
		members = new HashSet<AbstractAgent>(fitMembers);
		if (members.Count > maxContinuationCount) {
			fitMembers.Sort ((x, y) => y.fitness().CompareTo(x.fitness()));
			for (int i = maxContinuationCount; i < fitMembers.Count; i++)
				members.Remove (fitMembers [i]);
		}
		Debug.Log ("After Droopping unfit members " + toString());
	}

	public AbstractAgent getRandomMember() {
		double fitnessSum = 0.0f;
		var sumsToMemberIndexesMap = new Dictionary<double, int> ();
		var membersList = members.ToList ();
		for (int i = 0; i < memberCount(); i++) {
			if (membersList [i].fitness() <= 0)
				fitnessSum += 0.1f;
			else
				fitnessSum += membersList[i].fitness();
			sumsToMemberIndexesMap.Add (fitnessSum, i);
		}
		var cumulativeSumsList = sumsToMemberIndexesMap.Keys.ToList();
		return membersList [sumsToMemberIndexesMap [NeatUtils.getPseudoRandom (cumulativeSumsList)]];
	}

	public void selectRandomMaskotFromMembers() {
		maskot = members.ToList() [GlobalRandom.nextInt (members.Count)];
	}

	public void selectBestMemberAsMaskot() {
		maskot = bestAgent;
	}

	public string toString() {
		var memberNames = "";
		foreach (AbstractAgent agent in members)
			memberNames += agent.name() + " ,";
		return name + " mémber count: " + memberCount () + "; fitness: " + fitness + "; memberNames: " + memberNames;
	}
}
