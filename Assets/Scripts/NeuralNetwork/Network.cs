﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface AbstractActivationFunction {
	double calculate(double input);
}

public class LinearActivationFunction : AbstractActivationFunction {
	public double calculate(double input) {
		return input;
	}
}

public class StepActivationFunction : AbstractActivationFunction {
	public double calculate(double input) {
		if (input > 0)
			return 1.0f;
		return 0.0f;
	}
}

public class TanhActivationFunction : AbstractActivationFunction {
	public double calculate(double input) {
		return Math.Tanh (input);
	}
}

public class Neuron {
	private List<Connection> inputs = new List<Connection>();
	private Dictionary<Neuron, bool> outputs = new Dictionary<Neuron, bool>();
	private AbstractActivationFunction activationFunction = new TanhActivationFunction ();
	private double bias = 0.0f;
	private int id = 0;
	private NodeGene.TYPE type = NodeGene.TYPE.INPUT;
	private double value = 0.0f;
	private int layer = -1;
	private double inputValue = 0.0f;

	public int Id {
		get {
			return id;
		}
	}

	public NodeGene.TYPE Type {
		get {
			return type;
		}
	}

	public Dictionary<Neuron, bool> Outputs {
		get {
			return outputs;
		}
	}

	public double Value {
		get {
			return value;
		}
	}

	public int Layer {
		get {
			return layer;
		}
		set {
			layer = value;
		}
	}

	public List<Connection> Inputs {
		get {
			return inputs;
		}
	}

	public Neuron(NodeGene node) {
		this.id = node.Innovation;
		this.type = node.Type;
		this.bias = node.Bias;
	}

	public void setInputValue(double value) {
		inputValue = value;
	}

	public void addInput(Connection connection) {
		inputs.Add (connection);
		connection.InNode.addOutput (this, connection.IsRecurrent);
	}

	private void addOutput(Neuron outputNeuron, bool isRecurrent) {
		outputs.Add (outputNeuron, isRecurrent);
	}

	public bool connectedTo(Neuron other) {
		return outputs.ContainsKey (other);
	}

	public void calculate() {
		double inputSum = inputValue;
		foreach (Connection input in inputs) {
//			Debug.Log("Calculating " + id + " and adding " + input.InNode.id + " value: " + input.InNode.value + " with weight: " + input.Weight + " bias: " + bias);
			inputSum += input.InNode.value * input.Weight;
		}
		value = activationFunction.calculate (inputSum + bias);
//		Debug.Log (id + "Value is; " + value);
		inputValue = 0.0f;
	}

	public string toString() {
		string result = id + " value: " + value + ", layer: " + layer + ", outputs: " + " bias: " + bias;
		foreach (Neuron neuron in outputs.Keys)
			result += neuron.Id + ", ";
		result += " , inputs: ";
		foreach (Connection input in inputs)
			result += input.InNode.Id + ", ";
		
		return result;
	}
}

public class Connection {
	private Neuron inNode;
	private double weight;
	private bool isRecurrent;
	private bool isEnabled;

	public Neuron InNode {
		get {
			return inNode;
		}
	}

	public double Weight {
		get {
			return weight;
		}
	}

	public bool IsRecurrent {
		get {
			return isRecurrent;
		}
	}

	public Connection (Neuron inNode, double weight, bool isRecurrent, bool isEnabled) {
		this.inNode = inNode;
		this.weight = weight;
		this.isRecurrent = isRecurrent;
		this.isEnabled = isEnabled;
	}

	public string toString() {
		return "connection: " + inNode.Id + " isRecurrent: " + isRecurrent + " isEnabled: " + isEnabled + " weight: " + weight;
	}
}

public class NeuralNetwork {
	private List<Neuron> sortedNeurons = new List<Neuron>();
	private Dictionary<int, Neuron> neurons = new Dictionary<int, Neuron>();
	private List<Neuron> inputNeurons = new List<Neuron> ();
	private List<Neuron> outputNeurons = new List<Neuron> ();
	private Genome genome;

	public NeuralNetwork(Genome genome) {
		this.genome = genome;
		foreach (NodeGene node in genome.Nodes.Values)
			neurons.Add (node.Innovation, new Neuron (node));
		foreach (ConnectionGene connection in genome.Connections.Values)
			neurons [connection.OutNode].addInput (new Connection(neurons [connection.InNode], connection.Weight, connection.IsRecurrent, connection.Enabled));
		walkNetworkAndAssignLayers ();
		sortedNeurons = neurons.Values.ToList ();
		sortedNeurons.Sort ((first, second) => first.Layer.CompareTo (second.Layer));
		inputNeurons = getNeuronsOfType (NodeGene.TYPE.INPUT);
		inputNeurons.Sort ((first, second) => first.Id.CompareTo (second.Id));
		outputNeurons = getNeuronsOfType (NodeGene.TYPE.OUTPUT);
		outputNeurons.Sort ((first, second) => first.Id.CompareTo (second.Id));
	}

	public Genome Genome {
		get {
			return genome;
		}
	}

	public List<Neuron> Neurons {
		get {
			return neurons.Values.ToList();
		}
	}

	public int layerCount() {
		return sortedNeurons.Last ().Layer + 1;
	}

	public int layerOf (int nodeId) {
		return neurons [nodeId].Layer;
	}

	public double[] calculate(double[] inputValues) {
		if (inputNeurons.Count != inputValues.Length)
			throw new KeyNotFoundException ("Input values count does not match input count");

		for (int i = 0; i < inputNeurons.Count; i++)
			inputNeurons [i].setInputValue (inputValues [i]);

		foreach (Neuron neuron in sortedNeurons)
			neuron.calculate ();
		var result = new List<double> ();
		foreach (Neuron neuron in outputNeurons)
			result.Add (neuron.Value);
		return result.ToArray ();
	}

	private List<Neuron> getNeuronsOfType(NodeGene.TYPE type) {
		var result = new List<Neuron> ();
		foreach (Neuron neuron in neurons.Values)
			if (neuron.Type == type)
				result.Add (neuron);
		return result;
	}

	public string toString() {
		string result = "";
		foreach (int id in neurons.Keys)
			result += neurons[id].toString() + " ;; ";
		return result;
	}

	public void walkNetworkAndAssignLayers() {
		foreach (Neuron neuron in getNeuronsOfType (NodeGene.TYPE.INPUT))
			walkSubNetworkAndAssignLayers (neuron, 0);

		var maxLayer = -1;
		foreach (Neuron neuron in getNeuronsOfType (NodeGene.TYPE.OUTPUT))
			if (neuron.Layer > maxLayer)
				maxLayer = neuron.Layer;

		foreach (Neuron neuron in getNeuronsOfType (NodeGene.TYPE.OUTPUT))
			neuron.Layer = maxLayer;
	}

	public void walkSubNetworkAndAssignLayers(Neuron neuron, int layerToAssign) {
		if (neuron.Layer < layerToAssign)
			neuron.Layer = layerToAssign;

		foreach (KeyValuePair<Neuron, bool> output in neuron.Outputs) {
			if (output.Key != neuron && output.Value == false)
				walkSubNetworkAndAssignLayers (output.Key, layerToAssign + 1);
		}
	}
}
