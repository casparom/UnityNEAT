﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {
	public NetworkViewer viewer;
	public NetworkViewer viewer2;
	public NetworkViewer viewer3;

	private NeuralNetworkAgent agent1 = null;
	private NeuralNetworkAgent agent2 = null;
	private NeuralNetworkAgent agent3 = null;
	private NeuralNetwork network = null;

	private InnovationManager innovationManager;

	void Start () {
		innovationManager = new InnovationManager ();
		initialize ();
	}

	private void render() {
		viewer.renderNetwork (agent1.Network);
		viewer2.renderNetwork (agent2.Network);
	}

	public void mutate() {
		Debug.Log ("Starting to mutate first");
		agent1.mutate();
		Debug.Log ("Starting to mutate second");
		agent2.mutate ();
		render ();
	}

	public void initialize() {
		GlobalCounter.reset ();

		agent1 = new NorthMoverAgent(Genome.createMinimumGenome(innovationManager.getOrCreateInitialMutation(1, 1)), innovationManager, null);
		agent2 = agent1.copy() as NeuralNetworkAgent;
		render ();
	}

	public void crossOver() {
		agent3 = (NeuralNetworkAgent)agent1.crossOver (agent2);
		viewer3.renderNetwork (agent3.Network);
		agent1.calculateCompatibilityDistance (agent2);
		network = new NeuralNetwork (agent3.Genome);
	}

	public void calculate() {
		Debug.Log ("Before: " + network.toString());
		var result = network.calculate (new double[] {1});
		Debug.Log ("After: " + network.toString());
		printArray (result);
	}

	public void printArray(double[] array) {
		var contents = "";
		foreach (double item in array)
			contents += item +", ";
		Debug.Log (contents);
	}


}
	