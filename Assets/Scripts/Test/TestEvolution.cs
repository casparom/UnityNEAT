﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEvolution : MonoBehaviour {

	private AbstractAgentFactory agentFactory;
	private Evolutionizer evolutionizer;
	private InnovationManager innovationManager;

	void Start () {
		var startTime = DateTime.Now;
		innovationManager = new InnovationManager ();
		agentFactory = new RandomFitnessAgentFactory ();
		evolutionizer = new Evolutionizer (agentFactory);
		evolutionizer.setGenerationSize (1000);
		for (int i = 0; i < 1; i++) {
			evolutionizer.startSimulating ();
			evolutionizer.stopSimulating ();
			evolutionizer.speciate ();
			evolutionizer.calculateAdjustedFitnesses ();
			Debug.Log ("Best fitness so far: " + evolutionizer.getBestAgent ().fitness ());
			evolutionizer.createNewGeneration ();
		}
		var stepStartTime = DateTime.Now;
		for (int i = 0; i < 100; i++) {
			stepStartTime = DateTime.Now;
			evolutionizer.startSimulating ();
			printTimeToNow("startSimulating", stepStartTime);

			stepStartTime = DateTime.Now;
			evolutionizer.stopSimulating ();
			printTimeToNow("stopSimulating", stepStartTime);

			stepStartTime = DateTime.Now;
			evolutionizer.speciate ();
			printTimeToNow("speciate", stepStartTime);

			stepStartTime = DateTime.Now;
			evolutionizer.calculateAdjustedFitnesses ();
			printTimeToNow("calculateAdjustedFitnesses", stepStartTime);

			stepStartTime = DateTime.Now;
			evolutionizer.createNewGeneration ();
			printTimeToNow("createNewGeneration", stepStartTime);
		}
		printTimeToNow("Whole process", startTime);
	}

	private void printTimeToNow(string process, DateTime startTime) {
		Debug.Log(process + " took " + (DateTime.Now - startTime).TotalSeconds);
	}
}
