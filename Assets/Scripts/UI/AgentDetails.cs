﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AgentDetails : MonoBehaviour {
	public GameObject detailsPrefab;
	private AbstractAgent agent;
	private GameObject panel;
	private SpeciesViewer speciesViewer;

	void Start() {
		speciesViewer = GameObject.Find ("SpeciesViewer").GetComponent<SpeciesViewer> ();
	}

	public void OnDestroy() {
		if (panel)
			Destroy (panel);
	}

	public void OnMouseEnter() {
		panel = Instantiate (detailsPrefab);
		panel.transform.SetParent (GameObject.Find("Canvas").transform, false);
		positionDetailsPanel ();
		panel.GetComponentInChildren<Text> ().text = agent.toString();
		speciesViewer.highlightSpecies (agent.AgentSpecies);
	}

	public void OnMouseExit() {
		Destroy(panel);
		speciesViewer.unhighlightAll ();
	}

	public void setAgent(AbstractAgent agent) {
		this.agent = agent;
	}

	private void positionDetailsPanel() {
		panel.GetComponent<RectTransform> ().anchoredPosition = Input.mousePosition;
	}
}
