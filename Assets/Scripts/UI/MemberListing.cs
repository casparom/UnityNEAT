﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MemberListing : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
	private AbstractAgent member;
	private Color memberColor;

	public void setMember(AbstractAgent member) {
		this.member = member;
		GetComponentInChildren<Text> ().text = member.toString();
	}

	public void OnPointerEnter(PointerEventData eventData) {
		memberColor = member.AgentColor;
		member.AgentColor = Color.white;
	}

	public void OnPointerExit(PointerEventData eventData) {
		member.AgentColor = memberColor;
	}
}
