﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

class NeuronRepresentation {
	public Neuron neuron;
	public GameObject gameObject;

	public NeuronRepresentation(Neuron neuron, GameObject gameObject) {
		this.neuron = neuron;
		this.gameObject = gameObject;
	}
}

public class NetworkViewer : MonoBehaviour {

	private NeuralNetwork network = null;
	public GameObject neuronPrefab = null;
	public GameObject straightConnectionPrefab = null;
	public GameObject recurrentConnectionPrefab = null;
	Dictionary<int, GameObject> inputNeurons = new Dictionary<int, GameObject> ();
	Dictionary<int, GameObject> hiddenNeurons = new Dictionary<int, GameObject> ();
	Dictionary<int, GameObject> outputNeurons = new Dictionary<int, GameObject> ();
	Dictionary<int, NeuronRepresentation> allNeurons = new Dictionary<int, NeuronRepresentation> ();
	Dictionary<int, GameObject> connections = new Dictionary<int, GameObject> ();
	List<NeuronRepresentation> sortedNeuronRepresentations = new List<NeuronRepresentation> ();

	public void renderNetwork(NeuralNetwork network) {
		this.network = network;
		clearView ();
		showNetwork ();
	}

	private void clearView() {
		foreach (NeuronRepresentation rep in allNeurons.Values)
			Destroy (rep.gameObject);	
		inputNeurons.Clear ();
		hiddenNeurons.Clear ();
		outputNeurons.Clear ();
		allNeurons.Clear ();
		foreach (GameObject connection in connections.Values)
			Destroy (connection);	
		connections.Clear ();
	}

	private void showNetwork() {
		createNeurons ();
		sortedNeuronRepresentations = allNeurons.Values.ToList ();
		sortedNeuronRepresentations.Sort ((x, y) => x.neuron.Layer.CompareTo (y.neuron.Layer));
		positionNeurons ();
		createConnections ();
		setNeuronsInFront ();
	}

	private void setNeuronsInFront() {
		foreach(NeuronRepresentation neuron in allNeurons.Values) {
			neuron.gameObject.transform.SetAsLastSibling();
		}
	}

	private void createNeurons ()
	{
		foreach (Neuron neuron in network.Neurons) {
			addNeuron (neuron);
		}
	}

	private void positionNeurons() {
		var layerHeight = 250 / (network.layerCount() - 1);
		var layerNeurons = 0;
		var previousLayer = -1;
		for (int index = 0; index < sortedNeuronRepresentations.Count; index++) {
			var neuronRepresentation = sortedNeuronRepresentations [index];
			var layer = neuronRepresentation.neuron.Layer;
			if (layer != previousLayer)
				layerNeurons = 0;
			else
				layerNeurons++;
			previousLayer = layer;
			if (neuronRepresentation.neuron.Type == NodeGene.TYPE.INPUT)
				setElementPosition(neuronRepresentation.gameObject, new Vector2(30 + layerNeurons * 100, 30));
			else if (neuronRepresentation.neuron.Type == NodeGene.TYPE.OUTPUT)
				setElementPosition(neuronRepresentation.gameObject, new Vector2(30 + layerNeurons * 100, 30 + layerHeight * (network.layerCount() - 1)));
			else 
				setElementPosition(neuronRepresentation.gameObject, new Vector2(100 + layerNeurons * 50, 30 + layerHeight * layer));
		}
	}

	private void addNeuron (Neuron neuron)
	{
		allNeurons.Add(neuron.Id, new NeuronRepresentation(neuron, createElement(neuronPrefab, neuron.Id)));
	}

	private GameObject createElement(GameObject prefab, int innovation) {
		var element = Instantiate (prefab);
		element.transform.SetParent (transform, false);
		setElementText (element, innovation.ToString ());
		return element;
	}

	private void setElementText(GameObject element, string text) {
		element.GetComponentInChildren<Text> ().text = text;
	}

	private void setElementPosition(GameObject element, Vector2 position) {
		getRectTransform(element).anchoredPosition = position;
	}

	private RectTransform getRectTransform(GameObject element) {
		return element.GetComponent<RectTransform> ();
	}

	private void createConnections() {
		foreach (ConnectionGene con in network.Genome.Connections.Values) {
			GameObject newConnection = null;
			if (con.InNode == con.OutNode)
				newConnection = createRecurrentConnection (con);
			else
				newConnection = createStraightConnection (con);
			if (!con.Enabled)
				newConnection.GetComponent<Image> ().color = new Color32 (255, 255, 255, 30);
			connections.Add(con.Innovation, newConnection);
		}
	}

	private GameObject createRecurrentConnection(ConnectionGene connection) {
		var con = createElement (recurrentConnectionPrefab, connection.Innovation);
		setRecurrentConnectionPosition (con, connection.InNode);
		return con;
	}

	private void setRecurrentConnectionPosition(GameObject arrowGo, int neuronId) {
		var neuron = allNeurons[neuronId];
		var neuronPos = getRectTransform (neuron.gameObject).anchoredPosition;
		var arrow = getRectTransform (arrowGo);
		arrow.anchoredPosition = neuronPos + new Vector2 (15, -15);
	}

	private GameObject createStraightConnection(ConnectionGene connection) {
		var con = createElement (straightConnectionPrefab, connection.Innovation);
		setStraightConnectionPosition (con, connection);
		return con;
	}

	private void setStraightConnectionPosition(GameObject arrowGo, ConnectionGene connection) {
		var pointA = getRectTransform (allNeurons[connection.InNode].gameObject).anchoredPosition;
		var pointB = getRectTransform (allNeurons[connection.OutNode].gameObject).anchoredPosition;
		var arrow = getRectTransform(arrowGo);
		Vector2 differenceVector = pointB - pointA;
		arrow.sizeDelta = new Vector2( differenceVector.magnitude - 20, 15);
		arrow.pivot = new Vector2(0, 0.5f);
		arrow.anchoredPosition = pointA;
		float angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;
		arrow.rotation = Quaternion.Euler(0,0, angle);
	}
}
