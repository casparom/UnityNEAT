﻿
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeciesDetails : MonoBehaviour {
	public GameObject memberListingPrefab;
	public Text title;
	private Species species;
	private Dictionary<AbstractAgent, GameObject> listings = new Dictionary<AbstractAgent, GameObject>();

	public void Update() {
		positionListings ();
	}

	public void setSpecies(Species species) {
		this.species = species;
		title.text = species.Name + " members: " + species.memberCount () + " fitness: " + species.Fitness;
		createMemberListings ();
		positionListings();
	}

	private void createMemberListings() {
		foreach (AbstractAgent member in species.Members)
			listings.Add(member, createMemberListing (member));
	}

	private GameObject createMemberListing(AbstractAgent member) {
		var listing = Instantiate (memberListingPrefab);
		listing.transform.SetParent (transform, false);
		listing.GetComponent<MemberListing> ().setMember(member);
		return listing;
	}

	private void positionListings() {
		var membersList = listings.Keys.ToList();
		membersList.Sort ((x, y) => y.fitness().CompareTo (x.fitness()));
		for (int i = 0; i < membersList.Count; i++)
			positionListing (listings[membersList[i]], i);
	}

	private void positionListing(GameObject listing, int row) {
		var width = ((RectTransform)listing.transform).rect.width;
		var position = new Vector2 (width / 2 + 10, -30 - 20 * row);
		listing.GetComponent<RectTransform> ().anchoredPosition = position;
	}

}
