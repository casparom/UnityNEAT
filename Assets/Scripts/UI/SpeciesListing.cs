﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SpeciesListing : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
	public GameObject detailsPrefab;
	private Species species;
	private GameObject details;

	public void setSpecies(Species species) {
		this.species = species;
	}

	public bool Highlight {
		set {
			if (value) {
				GetComponentInChildren<SpriteRenderer> ().color = Color.white;
				GetComponentInChildren<Text> ().color = Color.white;
			} else {
				GetComponentInChildren<SpriteRenderer>().color = species.Color;
				GetComponentInChildren<Text> ().color = Color.black;
			}
		}
	}

	public void update() {
		colorize ();
		GetComponentInChildren<Text> ().text = species.Name + " fitness: " + species.Fitness.ToString("F2");
	}

	private void colorize() {
		GetComponentInChildren<SpriteRenderer>().color = species.Color;
	}

	public void OnPointerEnter(PointerEventData eventData) {
		GetComponentInChildren<Text> ().enabled = false;
		transform.SetAsLastSibling ();
		createDetails ();
	}

	public void OnPointerExit(PointerEventData eventData) {
		Destroy(details);
		GetComponentInChildren<Text> ().enabled = true;
	}

	private void createDetails() {
		details = Instantiate (detailsPrefab);
		details.transform.SetParent (transform, false);
		details.GetComponent<SpeciesDetails> ().setSpecies(species);
	}


}
