﻿
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeciesViewer : MonoBehaviour {
	public GameObject listingPrefab = null;
	private Dictionary<Species, GameObject> listings = new Dictionary<Species, GameObject> ();

	public void renderSpecies(ICollection<Species> allSpecies) {
		Debug.Log ("SpeciesCount: " + allSpecies.Count);
		createNeededListings (allSpecies);
		deleteUnneededLisitings (allSpecies);
		Debug.Log ("Listings count: " + listings.Count);
		positionListings ();
		updateListings ();
	}

	public void highlightSpecies(Species species) {
		listings[species].GetComponent<SpeciesListing> ().Highlight = true;
	}

	public void unhighlightAll() {
		foreach (GameObject listing in listings.Values)
			listing.GetComponent<SpeciesListing> ().Highlight = false;
	}

	private void updateListings() {
		foreach (GameObject listing in listings.Values)
			listing.GetComponent<SpeciesListing> ().update ();
	}

	private void createNeededListings(ICollection<Species> allSpecies) {
		foreach (Species species in allSpecies)
			if (!listings.ContainsKey(species))
				listings.Add (species, createListing(species));
	}

	private GameObject createListing(Species species) {
		var listing = Instantiate (listingPrefab);
		listing.transform.SetParent (transform, false);
		listing.GetComponent<SpeciesListing> ().setSpecies(species);
		return listing;
	}

	private void deleteUnneededLisitings(ICollection<Species> allSpecies) {
		foreach (KeyValuePair<Species, GameObject> listing in listings)
			if (!allSpecies.Contains (listing.Key)) {
				Destroy (listing.Value);
				listings.Remove (listing.Key);
			}
	}

	private void positionListings() {
		Debug.Log ("Positioning");
		var listingsList = listings.Keys.ToList ();
		listingsList.Sort ((x, y) => y.Fitness.CompareTo (x.Fitness));
		for (int i = 0; i < listings.Count; i++) {
			positionListing (listings [listingsList [i]], i);

			Debug.Log ("FITNESS from getter iS: " + listingsList[i].Fitness.ToString("F2"));
		}
	}

	private void positionListing(GameObject listing, int row) {
		var width = ((RectTransform)listing.transform).rect.width;
		var position = new Vector2 (width / 2 + 10, -30 - 25 * row);
		listing.GetComponent<RectTransform> ().anchoredPosition = position;
	}
}
